#!/bin/sh

if [ -z "${PERSISTENT_PATH}" ]
then
	echo "PERSISTENT_PATH variable should be defined!"
	exit 1
fi

dhparamFile="${PERSISTENT_PATH}/dhparam.pem"
if [ ! -e "${dhparamFile}" ]
then
	echo "DHParam not found, generating.."
	openssl dhparam -out "${dhparamFile}" 4096
fi

configFiles=$(ls ${NGINX_CONFD_PATH}/*.conf)
for configFile in ${configFiles}
do
	envsubst "${ENVSUBST_VARS}" < ${configFile} > ${configFile}.tmp
	mv ${configFile}.tmp ${configFile}
done

streamConfigFiles=$(ls ${NGINX_STREAM_CONFD_PATH}/*.conf)
for streamConfigFile in ${streamConfigFiles}
do
	envsubst "${ENVSUBST_VARS}" < ${streamConfigFile} > ${streamConfigFile}.tmp
	mv ${streamConfigFile}.tmp ${streamConfigFile}
done

nginx -g 'daemon off;'
